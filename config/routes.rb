Rails.application.routes.draw do
 namespace :api do
  resources :user, :except => [:index]
  get 'user', to: "user#index"
  get 'typeahead/:input', to: 'user#typeahead'
 end
end
